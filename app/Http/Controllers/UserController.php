<?php

namespace App\Http\Controllers;

use App\Address;
use Illuminate\Auth\Events\Verified;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth' => 'verified']);
    }
}
