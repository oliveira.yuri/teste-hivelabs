<?php

namespace App\Http\Controllers;

use App\Address;
use Illuminate\Auth\Events\Verified;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth' => 'verified']);
    }

    public function index()
    {
        return view('addresses.index');
    }

    public function create()
    {
        return view('addresses.create');
    }

    public function store(Request $request)
    {
        $url = "viacep.com.br/ws/".$request->cep."/json/";
        $inicio = curl_init($url);
        curl_setopt($inicio, CURLOPT_RETURNTRANSFER, true);
        $resposta = curl_exec($inicio);
        $array = json_decode($resposta, true);
       
        if(isset($array["erro"])){
            return back()->withInput();
        }else{
            Address::create([
                "zipcode" => $array["cep"],
                "location" => $array["logradouro"],
                "neighborhood" => $array["bairro"],
                "city" => $array["localidade"],
                "state" => $array["uf"],
                "user_id" => \Auth::user()->id
            ]);

            curl_close($inicio);

            return redirect('address/show');
        }
    }

    public function show()
    {
        $addresses = Address::where("user_id", \Auth::user()->id)->get();
        
        return view('addresses.show', compact('addresses'));
    }
}
