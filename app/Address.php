<?php

namespace App;
use App\User;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    public $table = "address";
    
    protected $fillable = [
        'zipcode','location','number', 'neighborhood', 'city',
        'state', 'user_id'
    ];
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
