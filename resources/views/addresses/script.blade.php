@section('scripts')
<script type="text/javascript" src="{{asset('js/jquery-3.4.1.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.mask.min.js')}}"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>

 
<script>
    $(document).ready(function ($) { 
        var $seuCampoCpf = $("#cep");
        $seuCampoCpf.mask('99999-999', {reverse: true});
    });
</script>

<script>
    $(document).ready(function($) {
        $('#myTable').DataTable( {
        "bProcessing": true,
        "deferRender": true,
        "sPaginationType": "full_numbers",
            //Modifica a linguagem padrão.
            "language": {
                "sProcessing": "Processando...",
                "lengthMenu": "Exibindo  _MENU_  registros por página",
                "zeroRecords": "Nenhum registro foi encontrado",
                "info": "Exibindo _PAGE_ de _PAGES_ paginas",
                "infoEmpty": "Nenhum registro disponível",
                "sLoadingRecords": "Carregando...",
                "infoFiltered": "(Filtrado aplicado em _MAX_ registros)",
                "sSearch": "Pesquisar ",
                "oPaginate": {
                "sFirst": "Primeira",
                "sPrevious": "Anterior",
                "sNext": "Próxima",
                "sLast": "Última",
                },
            },
            //Remoção da Ordenação da coluna "ação".
            "columnDefs": [ {
            "targets": [  ],
            "orderable": false,
            }]
        } );
        $('.dataTables_filter input[type="search"]').
        attr('placeholder','').
        css({'width':'300px','display':'inline-block','font-family':'Roboto','font-size':'11px'});
    
        $('.table.dataTable thead>tr>th').
        attr('th','').
        css({'color':'#434343','font-family':'Roboto','font-weight':'bold','font-size':'11px'});
    });
</script>