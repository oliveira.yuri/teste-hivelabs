@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Cadastro de Endereço 
                    <a style="float:right;" title="Voltar" href="{{url('/')}}"><i class="fa fa-arrow-left"></i></a>
                </div>

            
                <div class="card-body">
                    <form class="js-validation-material form-horizontal m-t-sm" action="{{ url('address/store') }}"
                    file="true" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}

                        <div class="form-group row">
                            <label for="cep" class="col-md-2 col-form-label text-md-right">Cep</label>

                            <div class="col-md-6">
                                <input id="cep" type="text" placeholder="Forneça seu cpf" class="form-control @error('cep') is-invalid @enderror" name="cep" value="{{ old('cep') }}" required autocomplete="cep" autofocus>

                                @error('cep')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-md-4 ">
                                <button type="submit" class="btn btn-primary">
                                    Registrar
                                </button>
                            </div>
                        </div>
                    </form>
                    <div class="form-group row mb-0">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@include('addresses.script')