@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Início</div>

                <div class="card-body">
                    <div class="col-md-12">
                        <a href="{{url('/address/create')}}"><button type="button" class="btn btn-primary">Novo Endereço</button></a>
                        <a href="{{url('/address/show')}}"><button type="button" class="btn btn-success">Listar Endereços</button></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
