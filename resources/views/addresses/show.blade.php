@extends('layouts.app')

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Lista de Endereços
                    <a style="float:right;" title="Voltar" href="{{url('/')}}"><i class="fa fa-arrow-left"></i></a>
                </div>

                <div class="card-body">
                    <div class="col-md-12">
                            <table id="myTable" class="display" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Cep</th>
                                    <th>Rua</th>
                                    <th>Bairro</th>
                                    <th>Cidade</th>
                                    <th>Estado</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($addresses as $address)
                                <tr>
                                    <td>{{$address->zipcode}}</td>
                                    <td>{{$address->location}}</td>
                                    <td>{{$address->neighborhood}}</td>
                                    <td>{{$address->city}}</td>
                                    <td>{{$address->state}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@include('addresses.script')
