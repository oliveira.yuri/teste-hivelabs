<?php

use Illuminate\Http\Request;

Auth::routes(['verify' => true]);

Route::group(['middleware' => ['auth']], function(){
    Route::get('/', 'AddressController@index')->name('adress');
    Route::get('/address/create', 'AddressController@create');
    Route::post('/address/store', 'AddressController@store');
    Route::get('/address/show', 'AddressController@show');
});
